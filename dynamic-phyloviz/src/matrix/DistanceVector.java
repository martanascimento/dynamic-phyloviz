package matrix;

import java.io.Serializable;

public class DistanceVector implements Identifiable, Serializable{
	
	protected int uid;
	protected final String id;
	public float[] distances;
	
	public DistanceVector(int uid, String id, int size){
		this.uid = uid;
		this.id = id;
		this.distances = new float[size];
	}
	@Override
	public int getUID(){
		return this.uid;
	}
	@Override
	public String getID(){
		return this.id;
	}
	@Override
	public int getFreq() {
		return 1;
	}
	public void setDistances(float[] d){
		this.distances = d;
	}
	public void setDistance(int index, float d){
		this.distances[index] = d;
	}
	
	@Override
	public String toString(){
		return this.id;
	}
	@Override
	public void setUID(int uid) {
		this.uid = uid;
	}
}
