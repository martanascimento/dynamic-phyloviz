package matrix;

public interface Identifiable {

	public void setUID(int uid);
	public int getUID();
	public String getID();
	public int getFreq();
}
