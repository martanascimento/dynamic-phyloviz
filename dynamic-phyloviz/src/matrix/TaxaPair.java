package matrix;

import java.io.Serializable;

public class TaxaPair<T extends OTU> implements Serializable, Comparable<TaxaPair<T>> {
	protected T u, v;
	protected float distance;
	
	public TaxaPair(T u, T v) {
		this.u = u;
		this.v = v;
	}
	public TaxaPair(T u, T v, float distance) {
		this.u = u;
		this.v = v;
		this.distance = distance;
	}
	public T getU() {
		return u;
	}

	public T getV() {
		return v;
	}
	
	public float getDistance(){
		return distance;
	}

	public void setDistance(float d){
		this.distance = d;
	}

	@Override
	public int compareTo(TaxaPair<T> e) {
		if (u.getUID() - e.getU().getUID() != 0)
			return u.getUID() - e.getU().getUID();

		return v.getUID() - e.getV().getUID();
	}
}
