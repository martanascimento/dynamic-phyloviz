package matrix;

import java.io.Serializable;
import java.util.Arrays;

public abstract class OTU implements Serializable{
	private int uid;
	private String id;
	protected float[] distances;

	public int index;
	protected int offset;
	public Index indexes;
	
	public OTU(int uid, String id){
		this.uid = uid;
		this.id = id;
	}
	public void addIndex(Index i, int size){
		this.distances = new float[size - i.index - 1];
		this.index = i.index;
		this.offset = index + 1;
		this.indexes = i;
	}
	public int getUID() {
		return uid;
	}

	public String getID() {
		return id;
	}

	public int getNodeIdx() {
		return offset;
	}

	public void setDistance(int i, float value) {
		if(i - offset == distances.length){
			distances = Arrays.copyOf(distances, distances.length +1);
		}
		distances[i - offset] = value;
	}
	
	protected void empty() {
		distances = null;
		indexes = null;
	}
	public float[] getDistances() {
		return distances;
	}

	public float getDistance(int index) {
		return distances[index - offset];
	}
	@Override
	public String toString(){
		return this.id;
	}
	public abstract boolean isLeaf();
	public abstract int size();
	public abstract Identifiable getIdentifiable();
}
