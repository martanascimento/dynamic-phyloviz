package matrix.dissimilarity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import matrix.DistanceVector;

public class MatrixPairwiseDissimilarity<T extends DistanceVector> implements PairwiseDissimilarity<T> {

	private List<T> identifiable;
	private float[][] distances;
	private int maxLevel = 1;

	public MatrixPairwiseDissimilarity(Reader r, boolean add) {
		int size = 0;
		try (BufferedReader in = new BufferedReader(r)) {
			if (in.ready())
				size = Integer.parseInt(in.readLine());

			if (!add) {
				identifiable = new ArrayList<T>(size);
				distances = new float[size][size];

				String s = in.readLine();
				String[] line = s.split("[ ,\t]+", 0);

				int uid = 0;
				if (line.length == size)
					parseUpper(uid, line);
				else
					parse(uid, line);
				identifiable.add((T) new DistanceVector(uid++, line[0], size));
				while (in.ready()) {
					s = in.readLine();

					line = s.split("[ ,\t]+", 0);
					
					if (line.length == size)
						parseUpper(uid, line);
					else
						parse(uid, line);

					identifiable.add((T) new DistanceVector(uid++, line[0], size));
				}

				for (int i = 0; i < distances.length; i++) {
					identifiable.get(i).setDistances(distances[i]);
					distances[i] = null;
				}
				distances = null;
			
			} else {

				String s = in.readLine();
				String[] line = s.split("[ ,\t]+", 0);
				identifiable = new ArrayList<T>(1);
				identifiable.add((T) new DistanceVector(size-1, line[0], size));
				for (int i = 0; i < size; i++) {
					float d = Float.parseFloat(line[i+1]);
					if (maxLevel < d) {
						maxLevel = Math.round(d);
					}
					identifiable.get(0).setDistance(i, d);
				}
			}
		} catch (NumberFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void parseUpper(int id, String[] line) {
		for (int i = 1; i < line.length; i++) {
			distances[id][id + i] = Float.parseFloat(line[i]);
			distances[id + i][id] = distances[id][id + i];

			if (maxLevel < distances[id][i + 1]) {
				maxLevel = Math.round(distances[id][i + 1]);
			}
		}
	}

	private void parse(int id, String[] line) {
		for (int i = 1; i < line.length; i++) {
			distances[id][i - 1] = Float.parseFloat(line[i]);
			distances[i - 1][id] = distances[id][i - 1];

			if (maxLevel < distances[id][i - 1]) {
				maxLevel = Math.round(distances[id][i - 1]);
			}
		}
	}

	@Override
	public float distance(DistanceVector t1, DistanceVector t2) {
		if(t1.distances != null && t2.distances != null)
			if(t2.distances.length > t1.distances.length)
				return t2.distances[t1.getUID()];
		return t1.distances[t2.getUID()];
	}

	@Override
	public int size() {
		return identifiable.size();
	}

	@Override
	public T get(int i) {
		return identifiable.get(i);
	}

	@Override
	public List<T> getIdentifiable() {
		return identifiable;
	}

	public int maxLevel() {
		return this.maxLevel;
	}
	//
	// @Override
	// public void setMaxLevel(int lv) {
	// this.maxLevel = lv;
	//
	// }

}
