package matrix.dissimilarity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import algo.AbstractDistance;
import core.data.AbstractProfile;
import core.data.TypingData;
import goeburst.distance.HammingDistanceProvider;

public class ProfilePairwiseDissimilarity<T extends AbstractProfile> implements PairwiseDissimilarity<T> {
	
	private AbstractDistance<T> ad;
	private final List<T> identifiable = new ArrayList<T>();

	public ProfilePairwiseDissimilarity(TypingData<T> td, AbstractDistance<T> ad) {
		this.ad = ad;
		for (Iterator<T> it = td.iterator(); it.hasNext();) {
			identifiable.add(it.next());			
		}
	}

	public ProfilePairwiseDissimilarity(TypingData<T> td) {
		this(td, new HammingDistanceProvider<T>().getDistance());
	}

	@Override
	public int size() {
		return identifiable.size();
	}

	@Override
	public List<T> getIdentifiable() {
		return identifiable;
	}

	@Override
	public float distance(T t1, T t2) {
		return ad.level(t1, t2);
	}

	@Override
	public T get(int i) {
		return identifiable.get(i);
	}
}
