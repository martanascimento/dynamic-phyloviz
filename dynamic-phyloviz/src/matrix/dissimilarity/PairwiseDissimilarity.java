package matrix.dissimilarity;

import java.io.Serializable;
import java.util.List;

import matrix.Identifiable;

public interface PairwiseDissimilarity<T extends Identifiable> extends Serializable{
	public float distance(T t1, T t2);
	public int size();
	public T get(int i);
	public List<T> getIdentifiable();
//	public int maxLevel();
//	public void setMaxLevel(int lv);
//	public AbstractDistance getDistanceProvider();
//	public void setDistanceProvider(AbstractDistance ad);
}
