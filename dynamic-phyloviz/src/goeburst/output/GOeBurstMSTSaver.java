package goeburst.output;

import java.util.Collection;

import goeburst.cluster.Edge;
import goeburst.tree.GOeBurstNode;
import matrix.dissimilarity.PairwiseDissimilarity;

public class GOeBurstMSTSaver implements INodeSaver {

	private Collection<Edge<GOeBurstNode>> edges;
	private int level;
	private PairwiseDissimilarity<?> pd;

	public GOeBurstMSTSaver(Collection<Edge<GOeBurstNode>> edges, int level, PairwiseDissimilarity<?> pd) {
		this.edges = edges;
		this.level = level;
		this.pd = pd;
	}

	@Override
	public Object get() {
		return new Object[]{edges, level, pd};
	}

}
