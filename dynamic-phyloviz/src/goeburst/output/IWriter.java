package goeburst.output;

public interface IWriter {
	
	public String write(INodeSaver n);

}
