package goeburst.output.newick;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import goeburst.cluster.Edge;
import goeburst.cluster.GOeBurstClusterWithStats;
import goeburst.cluster.GOeBurstNodeExtended;
import goeburst.output.INodeSaver;
import matrix.Identifiable;
import matrix.dissimilarity.PairwiseDissimilarity;

public class GOeBurstNewickWriter extends NewickWriter {
int AI = 0;
	class Graph {
		private Map<Integer, List<GOeBurstNodeExtended>> adj;
		private int size;
		private GOeBurstNodeExtended root;

		public Graph(Collection<GOeBurstNodeExtended> nodes, ArrayList<Edge<GOeBurstNodeExtended>> edges) {
			adj = new HashMap<>();
			for (GOeBurstNodeExtended st : nodes) {
				adj.put(st.getUID(), new LinkedList<GOeBurstNodeExtended>());
			}
			
			size = 0;
			root = null;
			for (Edge<GOeBurstNodeExtended> edge : edges) {
				if(!edge.visible())	continue;
				
				addNeighbor(edge.getU(), edge.getV());
				addNeighbor(edge.getV(), edge.getU());
				if (getNeighbors(edge.getU()).size() > size) {
					size = getNeighbors(edge.getU()).size();
					root = edge.getU();
				}
				if (getNeighbors(edge.getV()).size() > size) {
					size = getNeighbors(edge.getV()).size();
					root = edge.getV();
				}
			}
		}

		public void addNeighbor(GOeBurstNodeExtended v1, GOeBurstNodeExtended v2) {
			adj.get(v1.getUID()).add(v2);
		}
		public List<GOeBurstNodeExtended> getNeighbors(GOeBurstNodeExtended v) {
			return adj.get(v.getUID());
		}
		public boolean contains(GOeBurstNodeExtended v){
			return adj.containsKey(v.getUID());
		}
	}

	@Override
	public String write(INodeSaver n) {
		Object[] obj = (Object[]) n.get();
		Collection<GOeBurstClusterWithStats> clustering = (Collection<GOeBurstClusterWithStats>) obj[0];
		PairwiseDissimilarity<Identifiable> pd = (PairwiseDissimilarity<Identifiable>) obj[1];
		
		StringBuilder sb = new StringBuilder();
		for (GOeBurstClusterWithStats cluster : clustering) {
			
			Graph g = new Graph(cluster.getSTs(), cluster.getEdges());
			GOeBurstNodeExtended root = g.root;

			sb.append("(");
			for (GOeBurstNodeExtended st : cluster.getSTs()) {
				if(g.getNeighbors(st).isEmpty())
					sb.append(st.getID()).append(",");
//					sb.append(st.getID()).append(",_),");
			}
			
			if (root != null && g.getNeighbors(root) != null) {
				int children = 0;
				for (Iterator<GOeBurstNodeExtended> it = g.getNeighbors(root).iterator(); it.hasNext(); children++) {
					GOeBurstNodeExtended id = it.next();
					sb.append(format(g, pd, id, root) + ":" + (int)pd.distance(root.getIdentifiable(), id.getIdentifiable()) + ",");
				}
				if(children == 1)
					sb.append("_,");
				sb.replace(sb.length() - 1, sb.length(), ")" + root.getID() + ",");
			}
			
			sb.replace(sb.length() - 1, sb.length(), ";\n");
		}
		return sb.toString();

	}

	private String format(Graph g, PairwiseDissimilarity<Identifiable> pd, GOeBurstNodeExtended curr, GOeBurstNodeExtended parent) {
		if (g.getNeighbors(curr).size() == 1)
			return curr.getID();
		
		StringBuilder s = new StringBuilder("(");
		int children = 0;
		for (Iterator<GOeBurstNodeExtended> it = g.getNeighbors(curr).iterator(); it.hasNext();) {
			GOeBurstNodeExtended id = it.next();
			if (id.getUID() == parent.getUID())
				continue;
			children++;
			s.append(format(g, pd, id, curr)).append(":").append((int)pd.distance(curr.getIdentifiable(), id.getIdentifiable())).append(",");
		}
		if(children == 1)
			s.append("_,");
		s.replace(s.length() - 1, s.length(), ")" + curr.getID());
		return s.toString();
	}

}
