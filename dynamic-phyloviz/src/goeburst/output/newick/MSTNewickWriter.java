package goeburst.output.newick;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import goeburst.cluster.Edge;
import goeburst.output.INodeSaver;
import goeburst.tree.GOeBurstNode;
import matrix.Identifiable;
import matrix.dissimilarity.PairwiseDissimilarity;

public class MSTNewickWriter extends NewickWriter {

	class Graph {
		private Map<Integer, List<GOeBurstNode>> adj;
		private int size;
		private GOeBurstNode root;

		public Graph(Collection<Edge<GOeBurstNode>> edges) {
			adj = new HashMap<>();
			size = 0;
			root = null;
			for (Edge<GOeBurstNode> edge : edges) {
				if(!adj.containsKey(edge.getU().getUID()))
					adj.put(edge.getU().getUID(), new LinkedList<GOeBurstNode>());
				if(!adj.containsKey(edge.getV().getUID()))
					adj.put(edge.getV().getUID(), new LinkedList<GOeBurstNode>());

				addNeighbor(edge.getU(), edge.getV());
				addNeighbor(edge.getV(), edge.getU());
				if (getNeighbors(edge.getU()).size() > size) {
					size = getNeighbors(edge.getU()).size();
					root = edge.getU();
				}
				if (getNeighbors(edge.getV()).size() > size) {
					size = getNeighbors(edge.getV()).size();
					root = edge.getV();
				}
			}
		}

		public void addNeighbor(GOeBurstNode v1, GOeBurstNode v2) {
			adj.get(v1.getUID()).add(v2);
		}

		public List<GOeBurstNode> getNeighbors(GOeBurstNode v) {
			return adj.get(v.getUID());
		}
	}

	@Override
	public String write(INodeSaver n) {
		Object[] obj = (Object[]) n.get();
		Collection<Edge<GOeBurstNode>> edges = (Collection<Edge<GOeBurstNode>>) obj[0];
		PairwiseDissimilarity<Identifiable> pd = (PairwiseDissimilarity<Identifiable>) obj[2];
		
		Graph g = new Graph(edges);
		GOeBurstNode root = g.root;

		StringBuilder sb = new StringBuilder("(");
		if (root != null && g.getNeighbors(root) != null) {
			for (Iterator<GOeBurstNode> it = g.getNeighbors(root).iterator(); it.hasNext(); ) {
				GOeBurstNode id = it.next();
				sb.append(format(g, id, root, pd) + ":" + (int)pd.distance(root.getIdentifiable(), id.getIdentifiable()) + ",");
			}
			sb.replace(sb.length() - 1, sb.length(), ")" + root.getID() + ",");
		}
		sb.replace(sb.length() - 1, sb.length(), ";\n");
		return sb.toString();

	}

	private String format(Graph g, GOeBurstNode curr, GOeBurstNode parent, PairwiseDissimilarity<Identifiable> pd) {
		if (g.getNeighbors(curr).size() == 1)
			return curr.getID();
		StringBuilder s = new StringBuilder("(");
		for (Iterator<GOeBurstNode> it = g.getNeighbors(curr).iterator(); it.hasNext();) {
			GOeBurstNode id = it.next();
			if (id.getUID() == parent.getUID())
				continue;
			s.append(format(g, id, curr, pd)).append(":").append((int)pd.distance(curr.getIdentifiable(), id.getIdentifiable())).append(",");
		}
		s.replace(s.length() - 1, s.length(), ")" + curr.getID());
		return s.toString();
	}

}
