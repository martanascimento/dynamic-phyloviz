package goeburst.output;

import java.util.Collection;

import goeburst.cluster.GOeBurstClusterWithStats;
import matrix.Identifiable;
import matrix.dissimilarity.PairwiseDissimilarity;

public class GOeBurstSaver implements INodeSaver {

	private Collection<GOeBurstClusterWithStats> clustering;
	PairwiseDissimilarity<? extends Identifiable> pd;

	public GOeBurstSaver(Collection<GOeBurstClusterWithStats> save, PairwiseDissimilarity<? extends Identifiable> pairwiseDissimilarity){
		this.clustering = save;
		this.pd = pairwiseDissimilarity;
	}
	
	@Override
	public Object get() {
		return new Object[]{clustering, pd};
	}

}
