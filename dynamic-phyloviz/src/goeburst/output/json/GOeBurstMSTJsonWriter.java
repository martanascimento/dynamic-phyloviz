/*-
 * Copyright (c) 2016, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net/>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package goeburst.output.json;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import goeburst.cluster.Edge;
import goeburst.output.INodeSaver;
import goeburst.tree.GOeBurstNode;

/**
 *
 * @author martanascimento
 */
public class GOeBurstMSTJsonWriter extends JsonWriter{
    
	private final StringBuilder nodes = new StringBuilder();
    private final StringBuilder edges = new StringBuilder();
    

    @Override
	protected Map<String, String> getObjects(INodeSaver n) {
    	Map<String, String> map = new LinkedHashMap<>();

        getInfo(n);
        
        map.put("nodes", nodes.toString());
        map.put("edges", edges.toString());
        
        return map;
        
	}    
    private void getInfo(INodeSaver n){
    	Object[] obj = (Object[]) n.get();
    	Collection<Edge<GOeBurstNode>> edgesCollection = (Collection<Edge<GOeBurstNode>>)obj[0];
    	int level = (int) obj[1];
        Map<Integer, GOeBurstNode> nodesMap = new HashMap<>();
        
        for (Edge<GOeBurstNode> edge : edgesCollection) {
            
            GOeBurstNode u = edge.getU();
            GOeBurstNode v = edge.getV();
            if(!nodesMap.containsKey(u.getUID())){
                
                String node = "\t\t{"   
                           + "\"id\": " + u.getUID() + ", "
                           + "\"profile\": \"" + u.getID() + "\", "
                           + "\"group-lvs\": "+ getLVs(u, level)  + "},\n ";
                nodes.append(node);
                nodesMap.put(u.getUID(), u);
            }
            if(!nodesMap.containsKey(v.getUID())){
                
                String node = "\t\t{"   
                           + "\"id\": " + v.getUID() + ", "
                           + "\"profile\": \"" + v.getID() + "\", "
                           + "\"group-lvs\": "+ getLVs(v, level)  + "},\n ";
                nodes.append(node);
                nodesMap.put(v.getUID(), v);
            }
            
            edges.append("\t\t{")
                .append("\"u\": ").append(u.getUID()).append(", ")
                .append("\"v\": ").append(v.getUID()).append("},\n ");
            
        }
        
    }
    private String getLVs(GOeBurstNode u, int level) {
        StringBuilder lvs = new StringBuilder("[");
        for(int i = 0; i < level; i++)
            lvs.append(u.getLV(i)).append(",");
        lvs.replace(lvs.length()-1, lvs.length(), "]");
        return lvs.toString();
    }
}
