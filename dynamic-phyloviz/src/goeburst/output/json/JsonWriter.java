package goeburst.output.json;

import java.util.Map;
import java.util.Set;

import goeburst.output.INodeSaver;
import goeburst.output.IWriter;

public abstract class JsonWriter implements IWriter{

	@Override
	public String write(INodeSaver n) {
		
		Map<String, String> map = getObjects(n);
		
		StringBuilder sb = new StringBuilder("{\n");
		Set<String> keys = map.keySet();
		for (String key : keys) {
			sb.append(setObjectArray(key, map.get(key)));
		}
		sb.append("}");

		String output = sb.toString();
		return output;

	}

	private String setObjectArray(String key, String value) {
		String result = "\t\"" + key + "\": [\n" 
			+ value.substring(0, value.length() - 2) // to remove last ','
			+ "\n\t],\n";
		return result;
	}

	protected abstract Map<String, String> getObjects(INodeSaver n);
}
