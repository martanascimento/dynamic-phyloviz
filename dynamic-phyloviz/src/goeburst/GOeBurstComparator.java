package goeburst;

import java.util.Comparator;

import goeburst.cluster.Edge;
import goeburst.tree.GOeBurstNode;
import matrix.Identifiable;
import matrix.OTUComparator;
import matrix.TaxaPair;
import matrix.dissimilarity.PairwiseDissimilarity;

public class GOeBurstComparator<T extends GOeBurstNode> implements OTUComparator<T> {

	private final Comparator<TaxaPair<T>> ecmp = new EdgeComparator();
	private final Comparator<T> pcmp = new IdentifiableComparator();
	private final PairwiseDissimilarity<Identifiable> pd;
	private int maxLevel = 0;

	public GOeBurstComparator(PairwiseDissimilarity<Identifiable> pd, int maxLevel) {
		this.pd = pd;
		this.maxLevel = maxLevel;
	}

	@Override
	public int compare(T n1, T n2) {
		return pcmp.compare(n1, n2);
	}

	public int compare(Edge<T> n1, Edge<T> n2) {
		return ecmp.compare(n1, n2);
	}

	@Override
	public int compare(TaxaPair<T> t) {
		return compare(t.getU(), t.getV());
	}

	private class IdentifiableComparator implements Comparator<T> {

		@Override
		public int compare(T u, T v) {
			return u.getUID() - v.getUID();
		}
	}

	private class EdgeComparator implements Comparator<TaxaPair<T>> {

		@Override
		public int compare(TaxaPair<T> f, TaxaPair<T> e) {
			int ret = 0;
			int k = 0;
			int lv = 0;

			ret = (int) (pd.distance(f.getU().getIdentifiable(), f.getV().getIdentifiable())
					- pd.distance(e.getU().getIdentifiable(), e.getV().getIdentifiable()));

			if (ret != 0) {
				return ret;
			}
			
			while (k < maxLevel) {

				ret = Math.max(f.getU().getLV(k), f.getV().getLV(k)) - Math.max(e.getU().getLV(k), e.getV().getLV(k));

				lv++;

				if (ret != 0) {
					break;
				}

				ret = Math.min(f.getU().getLV(k), f.getV().getLV(k)) - Math.min(e.getU().getLV(k), e.getV().getLV(k));

				lv++;

				if (ret != 0) {
					break;
				}

				k++;
			}

			/* SAT is ignored! */

			/* ST frequency. */
			if (k >= maxLevel) {

				lv = 2 * maxLevel + 2;

				ret = Math.max(f.getU().getIdentifiable().getFreq(), f.getV().getIdentifiable().getFreq())
						- Math.max(e.getU().getIdentifiable().getFreq(), e.getV().getIdentifiable().getFreq());

				lv++;

				if (ret == 0) {
					ret = Math.min(f.getU().getIdentifiable().getFreq(), f.getV().getIdentifiable().getFreq())
							- Math.min(e.getU().getIdentifiable().getFreq(), e.getV().getIdentifiable().getFreq());
					lv++;
				}
			}

			/* Decreasing... */
			ret *= -1;
			Comparator<String> cmp = new Comparator<String>() {

				@Override
				public int compare(String o1, String o2) {
					int size = o1.length() - o2.length();
					if (size != 0) {
						return size;
					}
					return o1.compareTo(o2);
				}
			};

			/* Last chance... */
			if (ret == 0) {

				String fMinId, fMaxId, eMinId, eMaxId;

				if (cmp.compare(f.getU().getID(), f.getV().getID()) < 0) {
					fMinId = f.getU().getID();
					fMaxId = f.getV().getID();
				} else {
					fMinId = f.getV().getID();
					fMaxId = f.getU().getID();
				}

				if (cmp.compare(e.getU().getID(), e.getV().getID()) < 0) {
					eMinId = e.getU().getID();
					eMaxId = e.getV().getID();
				} else {
					eMinId = e.getV().getID();
					eMaxId = e.getU().getID();
				}

				ret = fMinId.compareTo(eMinId);

				if (ret == 0)
					ret = fMaxId.compareTo(eMaxId);

				lv += 2;
			}

			return Integer.signum(ret) * lv;
		}
	}

	@Override
	public Comparator<TaxaPair<T>> getEdgeComparator() {
		return ecmp;
	}

	@Override
	public Comparator<T> getProfileComparator() {
		return pcmp;
	}
	
	@Override
	public int maxLevel() {
		return maxLevel;
	}
}
