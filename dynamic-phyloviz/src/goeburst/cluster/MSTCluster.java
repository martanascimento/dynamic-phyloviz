package goeburst.cluster;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import matrix.OTU;

public class MSTCluster<T extends OTU> implements Serializable{
	public List<Edge<T>> edges;
	
	public MSTCluster() {
		edges = new ArrayList<Edge<T>>();
	}
	
	public List<Edge<T>> getEdges() {
		return edges;
	}

	public void add(Edge<T> e) {
			edges.add(e);
	}
	public void add(List<Edge<T>> list) {
		edges.addAll(list);
	}
}
