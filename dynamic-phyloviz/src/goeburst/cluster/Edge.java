package goeburst.cluster;

import matrix.OTU;
import matrix.TaxaPair;

public class Edge<T extends OTU> extends TaxaPair<T>{

	private boolean visible = false;
	
	public Edge(T u, T v) {
		super(u, v);
		if (u.getUID() > v.getUID()) {
			this.u = v;
			this.v = u;
		}
	}
	
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean visible() {
		return visible;
	}
	
	public T getU(){
		return this.u;
	}
	public T getV(){
		return this.v;
	}
	public String toString(){
		return "("+ u +", "+ v +")";
	}
}
