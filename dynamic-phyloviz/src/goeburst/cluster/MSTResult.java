package goeburst.cluster;

import java.io.Serializable;
import java.util.Collection;
import java.util.TreeSet;

import matrix.OTU;

public class MSTResult<T extends OTU> implements Serializable{

	public Collection<Edge<T>> mst;
	public MSTCluster<T>[][] clusters;
	public TreeSet<Integer>[] clustersIndex;
	public final int size;

	public MSTResult(MSTCluster<T>[][] clusters, TreeSet<Integer>[] clustersIndex, Collection<Edge<T>> mst, int size) {
		this.clusters = clusters;
		this.clustersIndex = clustersIndex;
		this.mst = mst;
		this.size = size;
	}
	public MSTResult(Collection<Edge<T>> mst, int size){
		this.mst = mst;
		this.size = size;
	}
	public int size() {
		return size;
	}
}