/*-
 * Copyright (c) 2011, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole combination.
 * 
 * As a special exception, the copyright holders of this library give you
 * permission to link this library with independent modules to produce an
 * executable, regardless of the license terms of these independent modules,
 * and to copy and distribute the resulting executable under terms of your
 * choice, provided that you also meet, for each linked independent module,
 * the terms and conditions of the license of that module.  An independent
 * module is a module which is not derived from or based on this library.
 * If you modify this library, you may extend this exception to your version
 * of the library, but you are not obligated to do so.  If you do not wish
 * to do so, delete this exception statement from your version.
 */

package goeburst.tree;

import java.util.Collection;

import goeburst.cluster.Edge;
import goeburst.cluster.MSTResult;
import goeburst.output.Format;
import goeburst.output.GOeBurstMSTSaver;
import matrix.Identifiable;
import matrix.Matrix;
import matrix.dissimilarity.PairwiseDissimilarity;
import project.ProjectItem;

public class GOeBurstMSTResult implements ProjectItem {

	private Matrix<GOeBurstNode> m;
	private int level;
	private MSTResult<GOeBurstNode> result;
	private PairwiseDissimilarity<Identifiable> pd;

	public GOeBurstMSTResult(MSTResult<GOeBurstNode> result, Matrix<GOeBurstNode> m, int level) {
		this.result = result;
		this.m = m;
		this.pd = m.getPairwiseDissimilarity();
		this.level = level;
	}

	public Collection<Edge<GOeBurstNode>> getEdges() {
		return result.mst;
	}
	public MSTResult<GOeBurstNode> getResult(){
		return this.result;
	}
	public Matrix<GOeBurstNode> getMatrix(){
		return this.m;
	}
	@Override
	public String toString() {
		return "goeBURST Full MST";
	}

    @Override
    public String getMethodProviderName() {
        return "goeburstFullMST";
    }

    @Override
    public String getFactoryName() {
        return "GOeBurstMSTItemFactory";
    }

    @Override
    public String getOutput(Format f) {
    	String output = f.format(this, new GOeBurstMSTSaver(result.mst, level, pd));
        return output;
    }

    @Override
    public String getAlgorithmLevel() {
        return String.valueOf(level);
    }

	public PairwiseDissimilarity<Identifiable> getPairwiseDissimilarity() {
		return pd;
	}
}
