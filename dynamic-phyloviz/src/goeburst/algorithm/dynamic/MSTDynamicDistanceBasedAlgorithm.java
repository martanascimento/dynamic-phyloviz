/*-
 * Copyright (c) 2011, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole combination.
 * 
 * As a special exception, the copyright holders of this library give you
 * permission to link this library with independent modules to produce an
 * executable, regardless of the license terms of these independent modules,
 * and to copy and distribute the resulting executable under terms of your
 * choice, provided that you also meet, for each linked independent module,
 * the terms and conditions of the license of that module.  An independent
 * module is a module which is not derived from or based on this library.
 * If you modify this library, you may extend this exception to your version
 * of the library, but you are not obligated to do so.  If you do not wish
 * to do so, delete this exception statement from your version.
 */

package goeburst.algorithm.dynamic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import goeburst.cluster.Edge;
import matrix.Matrix;
import matrix.OTU;
import matrix.OTUComparator;

public class MSTDynamicDistanceBasedAlgorithm<T extends OTU> {

	private OTUComparator<T> cmp;
	private Graph g;
	private Collection<Edge<T>> tree;

	public MSTDynamicDistanceBasedAlgorithm(Matrix<T> m, Collection<Edge<T>> tree) {

		this.cmp = m.getComparator();
		this.tree = tree;
		this.g = new Graph(tree);
	}

	public Collection<Edge<T>> add(List<Edge<T>> edgesToAdd, T p) {
		Edge<T> first = edgesToAdd.remove(0);
		tree.add(first);
		g.addNeighbor(first);

		for (Edge<T> e : edgesToAdd) {

			List<Edge<T>> path = bfs(e);
			
			path.add(e);
			tree.add(e);
			g.addNeighbor(e);

			Collections.sort(path, cmp.getEdgeComparator());

			Edge<T> edgeToRemove = path.get(path.size() - 1);
			tree.remove(edgeToRemove);
			g.removeNeighbor(edgeToRemove);
		}

		return tree;
	}

	private List<Edge<T>> bfs(Edge<T> edge) {
		Queue<T> queue = new LinkedList<>();
		boolean[] visited = new boolean[g.size()];
		int[] parent = new int[g.size()];

		List<Edge<T>> path = new ArrayList<>();

		queue.add(edge.getU());
		parent[edge.getU().getUID()] = -1;

		while (!queue.isEmpty()) {
			T u = queue.poll();
			visited[u.getUID()] = true;

			for (T neighbor : g.getNeighbors(u)) {

				if (neighbor.equals(edge.getV())) { // cycle detected
					int v = neighbor.getUID();
					int id = u.getUID();
					while (id != -1) {
						path.add(g.getEdge(id, v));
						v = id;
						id = parent[v];
					}
					return path;
				}
				if (!visited[neighbor.getUID()]) {
					queue.add(neighbor);
					parent[neighbor.getUID()] = u.getUID();
				}
			}
		}
		return path;
	}

	class Graph {
		private Map<String, List<T>> adj;
		private Map<Integer, Map<Integer, Edge<T>>> edges;
		private int size = 0;
		public Graph() {
			adj = new HashMap<>();
			edges = new HashMap<>();
		}
		public Graph(Collection<Edge<T>> edges) {
			this();
			
			for (Edge<T> edge : edges) {

				addNeighbor(edge);
			}
		}
		public void create(Edge<T> edge){
			if (!adj.containsKey(edge.getU().getUID())) {
				adj.put(edge.getU().getID(), new LinkedList<T>());
				edges.put(edge.getU().getUID(), new HashMap<>());
				size++;
			}
			if (!adj.containsKey(edge.getV().getUID())) {
				adj.put(edge.getV().getID(), new LinkedList<T>());
				edges.put(edge.getV().getUID(), new HashMap<>());
				size++;
			}
		}

		public void addNeighbor(Edge<T> e) {
			
			create(e);
			
			adj.get(e.getU().getID()).add(e.getV());
			edges.get(e.getU().getUID()).put(e.getV().getUID(), e);

			adj.get(e.getV().getID()).add(e.getU());
			edges.get(e.getV().getUID()).put(e.getU().getUID(), e);
		}

		public List<T> getNeighbors(T v) {
			return adj.get(v.getID());
		}

		public Edge<T> getEdge(int u, int v) {
			return edges.get(u).get(v);
		}
		public void removeNeighbor(Edge<T> edge) {
			adj.get(edge.getU().getID()).remove(edge.getV());
			adj.get(edge.getV().getID()).remove(edge.getU());

			edges.get(edge.getU().getUID()).remove(edge.getV());
			edges.get(edge.getV().getUID()).remove(edge.getU());
			
		}
		public int size() {
			return size;
		}
	}
	public boolean exists(String id) {
		return g.adj.containsKey(id);
	}
}
