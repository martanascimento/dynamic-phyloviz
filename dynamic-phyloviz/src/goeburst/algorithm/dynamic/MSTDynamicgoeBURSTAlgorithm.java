/*-
 * Copyright (c) 2011, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole combination.
 * 
 * As a special exception, the copyright holders of this library give you
 * permission to link this library with independent modules to produce an
 * executable, regardless of the license terms of these independent modules,
 * and to copy and distribute the resulting executable under terms of your
 * choice, provided that you also meet, for each linked independent module,
 * the terms and conditions of the license of that module.  An independent
 * module is a module which is not derived from or based on this library.
 * If you modify this library, you may extend this exception to your version
 * of the library, but you are not obligated to do so.  If you do not wish
 * to do so, delete this exception statement from your version.
 */

package goeburst.algorithm.dynamic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import algo.util.DisjointSet;
import goeburst.algorithm.EdgesIterator;
import goeburst.cluster.Edge;
import goeburst.cluster.MSTCluster;
import goeburst.cluster.MSTResult;
import matrix.Matrix;
import matrix.OTU;
import matrix.OTUComparator;

public class MSTDynamicgoeBURSTAlgorithm<T extends OTU> {

	private Matrix<T> m;
	private OTUComparator<T> cmp;
	private Graph g;

	public MSTDynamicgoeBURSTAlgorithm(Matrix<T> m, Collection<Edge<T>> tree) {

		this.m = m;
		this.cmp = m.getComparator();
		this.g = new Graph(tree);
	}

	public MSTResult<T> add(List<Edge<T>> newEdges, MSTResult<T> ret, T newNode) {
		g.addNeighbors(newEdges);

		List<Edge<T>> mst = new ArrayList<>();
		DisjointSet set = new DisjointSet(m.size() - 1);

		MSTCluster<T>[][] clusters = new MSTCluster[cmp.maxLevel()][m.size()];
		TreeSet<Integer>[] clustersIndex = new TreeSet[cmp.maxLevel()];

		int level = 1;
		for (int i = 0; i < newEdges.size();) {
			Edge<T> e = newEdges.get(i);
			TreeSet<Edge<T>> possibleEdges = new TreeSet<>(cmp.getEdgeComparator());
			while (level == m.getDistance(e)) {
				int u = set.findSet(e.getU().getUID()), v = set.findSet(e.getV().getUID());

				if (u != v) { // not same set
					possibleEdges.add(e);
				}
				++i;

				if (i == newEdges.size())
					break;

				e = newEdges.get(i);
			}

			if (level < ret.clustersIndex.length && ret.clustersIndex[level - 1] != null) {
				Iterator<Integer> it = ret.clustersIndex[level - 1].iterator();
				while (it.hasNext()) {

					int j = it.next();
					for (Edge<T> ed : ret.clusters[level - 1][j].getEdges()) {
						int u = ed.getU().getUID(), v = ed.getV().getUID();
						if (!set.sameSet(u, v)) {
							possibleEdges.add(ed);
						}
					}
				}
			}
			for (Edge<T> edge : possibleEdges) {
				int u = edge.getU().getUID(), v = edge.getV().getUID();
				if (clusters[level - 1][u] == null) {
					clusters[level - 1][u] = new MSTCluster<>();
					if (clustersIndex[level - 1] == null)
						clustersIndex[level - 1] = new TreeSet<>();
				}
				if (clusters[level - 1][v] == null) {
					clusters[level - 1][v] = new MSTCluster<>();
					if (clustersIndex[level - 1] == null)
						clustersIndex[level - 1] = new TreeSet<>();
				}

				clusters[level - 1][u].add(relevantEdges(edge.getU(), level));
				clustersIndex[level - 1].add(u);
				clusters[level - 1][v].add(relevantEdges(edge.getV(), level));
				clustersIndex[level - 1].add(v);

				if (!set.sameSet(u, v)) {
					int uset = set.findSet(u);
					int vset = set.findSet(v);

					mst.add(edge);
					set.unionSet(u, v);

					int newSet = set.findSet(u);

					if (clusters[level - 1][newSet] == null) {
						clusters[level - 1][newSet] = new MSTCluster<>();
						if (clustersIndex[level - 1] == null)
							clustersIndex[level - 1] = new TreeSet<>();
					}
					clustersIndex[level - 1].add(newSet);

					if (uset != newSet && clusters[level - 1][uset] != null) {
						clusters[level - 1][newSet].add(clusters[level - 1][uset].edges);
						clusters[level - 1][uset] = null;
						clustersIndex[level - 1].remove(uset);
					}
					if (vset != newSet && clusters[level - 1][vset] != null) {
						clusters[level - 1][newSet].add(clusters[level - 1][vset].edges);
						clusters[level - 1][vset] = null;
						clustersIndex[level - 1].remove(vset);
					}
				}
			}

			++level;
		}
		return new MSTResult<T>(clusters, clustersIndex, mst, m.size());
	}

	private List<Edge<T>> relevantEdges(T n, int level) {
		List<Edge<T>> relevantEdges = new ArrayList<>();

		EdgesIterator<T> it = g.getNeighbors(n);
		while (it.hasNext()) {
			Edge<T> nb = it.next(level);
			if (nb == null)
				break;
			relevantEdges.add(nb);
		}

		return relevantEdges;

	}

	class Graph {
		
		private Map<String, TreeSet<Edge<T>>> adj;
		private int size = 0;
		
		public Graph() {
			this.adj = new HashMap<>();
		}

		public Graph(Collection<Edge<T>> edges) {
			this();

			for (Edge<T> edge : edges) {
				create(edge);
				addNeighbor(edge);
			}
		}

		public void create(Edge<T> edge) {
			if (!adj.containsKey(edge.getU().getID())) {
				adj.put(edge.getU().getID(), new TreeSet<Edge<T>>(cmp.getEdgeComparator()));
				size++;
			}
			if (!adj.containsKey(edge.getV().getID())) {
				adj.put(edge.getV().getID(), new TreeSet<Edge<T>>(cmp.getEdgeComparator()));
				size++;
			}
		}

		public void addNeighbor(Edge<T> e) {
			create(e);

			adj.get(e.getU().getID()).add(e);
			adj.get(e.getV().getID()).add(e);

		}

		public void addNeighbors(List<Edge<T>> edges) {
			for (Edge<T> e : edges) {
				addNeighbor(e);
			}

		}

		public EdgesIterator<T> getNeighbors(T v) {
			return new EdgesIterator<T>(adj.get(v.getID()), v);
		}
		
		public int size(){
			return this.size;
		}
	}

	public boolean exists(String id) {
		return g.adj.containsKey(id);
	}
}
