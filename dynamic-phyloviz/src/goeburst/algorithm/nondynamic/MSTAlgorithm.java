/*-
 * Copyright (c) 2011, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole combination.
 * 
 * As a special exception, the copyright holders of this library give you
 * permission to link this library with independent modules to produce an
 * executable, regardless of the license terms of these independent modules,
 * and to copy and distribute the resulting executable under terms of your
 * choice, provided that you also meet, for each linked independent module,
 * the terms and conditions of the license of that module.  An independent
 * module is a module which is not derived from or based on this library.
 * If you modify this library, you may extend this exception to your version
 * of the library, but you are not obligated to do so.  If you do not wish
 * to do so, delete this exception statement from your version.
 */

package goeburst.algorithm.nondynamic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import algo.util.DisjointSet;
import goeburst.algorithm.EdgesIterator;
import goeburst.cluster.Edge;
import goeburst.cluster.MSTCluster;
import goeburst.cluster.MSTResult;
import matrix.Matrix;
import matrix.OTU;
import matrix.OTUComparator;

public class MSTAlgorithm<T extends OTU> {

	private Matrix<T> m;
	private OTUComparator<T> cmp;
	private Graph g;

	public MSTAlgorithm(Matrix<T> m) {

		this.m = m;
		this.cmp = m.getComparator();

		this.g = new Graph();
		for (int i = 0; i < m.size(); i++) {
			for (int j = i + 1; j < m.size(); j++) {
				T u = m.get(i);
				T v = m.get(j);

				Edge<T> e = new Edge<T>(u, v);
				int lv = (int) m.getDistance(u, v);
				e.setDistance(lv);
				if (lv <= cmp.maxLevel()) {
					g.addNeighbor(e);
				}
			}
		}
	}

	public MSTResult<T> getTree() {

		DisjointSet set = new DisjointSet(m.size());
		List<Edge<T>> mst = new ArrayList<>();
		MSTCluster<T>[][] clusters = new MSTCluster[cmp.maxLevel()][m.size()];
		TreeSet<Integer>[] clustersIndex = new TreeSet[cmp.maxLevel()];
		
		TreeSet<Edge<T>> possibleEdges = new TreeSet<>(cmp.getEdgeComparator());
		int level = 1;
		while (mst.size() < m.size() - 1 && level <= cmp.maxLevel()) {

			Iterator<T> it = m.iterator();
			while (it.hasNext()) {
				T n = it.next();

				EdgesIterator<T> nbIt = g.getNeighborsEdges(n);
				while (nbIt.hasNext()) {
					Edge<T> e = nbIt.next(level);
					if (e == null) {
						break;
					}
					int v = e.getU().getUID(), w = e.getV().getUID();
					int i = set.findSet(v), j = set.findSet(w);
					if (i == j) { // same tree
						continue;
					}
					possibleEdges.add(e);
				}
			}
			if (possibleEdges.isEmpty()) {
				level++;
				continue;
			}

			for (Edge<T> e : possibleEdges) {
				int u = e.getU().getUID(), v = e.getV().getUID();

				if (clusters[level - 1][u] == null) {
					clusters[level - 1][u] = new MSTCluster<>();
					if (clustersIndex[level - 1] == null)
						clustersIndex[level - 1] = new TreeSet<>();
				}
				if (clusters[level - 1][v] == null) {
					clusters[level - 1][v] = new MSTCluster<>();
					if (clustersIndex[level - 1] == null)
						clustersIndex[level - 1] = new TreeSet<>();
				}

				clusters[level - 1][u].add(relevantEdges(e.getU(), level));
				clustersIndex[level - 1].add(u);
				clusters[level - 1][v].add(relevantEdges(e.getV(), level));
				clustersIndex[level - 1].add(v);
				
				if (!set.sameSet(u, v)) {
					int uset = set.findSet(u);
					int vset = set.findSet(v);
					
					mst.add(e);
					set.unionSet(u, v);
					
					int newSet = set.findSet(u);

					if (clusters[level - 1][newSet] == null) {
						clusters[level - 1][newSet] = new MSTCluster<>();
						if (clustersIndex[level - 1] == null)
							clustersIndex[level - 1] = new TreeSet<>();
					}
					clustersIndex[level - 1].add(newSet);

					if (uset != newSet && clusters[level - 1][uset] != null) {
						clusters[level - 1][newSet].add(clusters[level - 1][uset].edges);
						clusters[level - 1][uset] = null;
						clustersIndex[level - 1].remove(uset);
					}
					if (vset != newSet && clusters[level - 1][vset] != null) {
						clusters[level - 1][newSet].add(clusters[level - 1][vset].edges);
						clusters[level - 1][vset] = null;
						clustersIndex[level - 1].remove(vset);
					}
					
					
				}
			}
			possibleEdges = new TreeSet<>(cmp.getEdgeComparator());
		}

		return new MSTResult<T>(clusters, clustersIndex, mst, m.size());
	}
	private List<Edge<T>> relevantEdges(T n, int level) {
		List<Edge<T>> relevantEdges = new ArrayList<>();

		EdgesIterator<T> it = g.getNeighborsEdges(n);
		while (it.hasNext()) {
			Edge<T> nb = it.next(level);
			if (nb == null)
				break;
			relevantEdges.add(nb);
		}

		return relevantEdges;

	}
	class Graph {
		private Map<Integer, TreeSet<Edge<T>>> adjacencies;
//		private Map<Integer, EdgesIterator<T>> iters;

		public Graph() {
			this.adjacencies = new HashMap<>();
//			this.iters = new HashMap<>();
		}

		public Graph(Collection<Edge<T>> edges) {
			this();

			for (Edge<T> edge : edges) {
				create(edge);
				addNeighbor(edge);
			}
		}

		public void create(Edge<T> edge) {
			if (!adjacencies.containsKey(edge.getU().getUID())) {
				TreeSet<Edge<T>> set = new TreeSet<Edge<T>>(cmp.getEdgeComparator());
				adjacencies.put(edge.getU().getUID(), set);
			}
			if (!adjacencies.containsKey(edge.getV().getUID())) {
				TreeSet<Edge<T>> set = new TreeSet<Edge<T>>(cmp.getEdgeComparator());
				adjacencies.put(edge.getV().getUID(), set);
			}
		}

		public void addNeighbor(Edge<T> e) {
			create(e);

			adjacencies.get(e.getU().getUID()).add(e);
			adjacencies.get(e.getV().getUID()).add(e);
			
		
		}
		public void addNeighbors(List<Edge<T>> edges) {
			for (Edge<T> e : edges) {
				addNeighbor(e);
			}

		}
		public EdgesIterator<T> getNeighborsEdges(T v) {
//			if(!iters.containsKey(v.getUID()))
//				iters.put(v.getUID(), new EdgesIterator<>(adjacencies.get(v.getUID()), v));
//			return iters.get(v.getUID());
			return new EdgesIterator<>(adjacencies.get(v.getUID()), v);
		}
	}
}