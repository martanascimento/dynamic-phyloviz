/*-
 * Copyright (c) 2011, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole combination.
 * 
 * As a special exception, the copyright holders of this library give you
 * permission to link this library with independent modules to produce an
 * executable, regardless of the license terms of these independent modules,
 * and to copy and distribute the resulting executable under terms of your
 * choice, provided that you also meet, for each linked independent module,
 * the terms and conditions of the license of that module.  An independent
 * module is a module which is not derived from or based on this library.
 * If you modify this library, you may extend this exception to your version
 * of the library, but you are not obligated to do so.  If you do not wish
 * to do so, delete this exception statement from your version.
 */
package goeburst.algorithm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import algo.util.DisjointSet;
import goeburst.GOeBurstComparator;
import goeburst.cluster.Edge;
import goeburst.cluster.GOeBurstCluster;
import goeburst.cluster.GOeBurstClusterWithStats;
import goeburst.cluster.GOeBurstNodeExtended;
import matrix.Identifiable;
import matrix.Matrix;
import matrix.dissimilarity.PairwiseDissimilarity;

public class GOeBurstWithStats implements ClusteringMethod<GOeBurstClusterWithStats, Identifiable> {
	private class STLV {
		
		int[] lv = new int[GOeBurstClusterWithStats.MAXLV + 1];
	}

    private int level;
    private int maxStId;
    private TreeMap<Integer, STLV> stLVs;
	private Matrix<GOeBurstNodeExtended> m;

    public GOeBurstWithStats(PairwiseDissimilarity<Identifiable> pd, int level) {
        this.level = level;
        this.maxStId = 0;
        this.stLVs = new TreeMap<Integer, STLV>();
        
        List<GOeBurstNodeExtended> nodes = new ArrayList<>();
		for(int i = 0; i < pd.size(); i++){
			nodes.add(new GOeBurstNodeExtended(pd.get(i)));
		}
		GOeBurstComparator<GOeBurstNodeExtended> cmp = new GOeBurstComparator<GOeBurstNodeExtended>(pd, Math.max(GOeBurstCluster.MAXLV, 3));
		this.m = new Matrix<GOeBurstNodeExtended>(nodes, pd, cmp);
    }

//    public void addSTlvs(GOeBurstNodeExtended u) {
//
//        STLV uLV = stLVs.get(u.getUID());
//
//        if (uLV == null) {
//            uLV = new STLV();
//            stLVs.put(u.getUID(), uLV);
//        }
//    }
//
//    public void setSTlvs(GOeBurstNodeExtended n, int slv, int dlv, int tlv, int sat){
//        STLV st = stLVs.get(n.getUID());
//        st.lv[0] = slv;
//        st.lv[1] = dlv;
//        st.lv[2] = tlv;
//        st.lv[3] = sat;
//    }
    
    @Override
    public Collection<GOeBurstClusterWithStats> getClustering() {

        ArrayList<Edge<GOeBurstNodeExtended>> edges = getEdges();
        Collection<GOeBurstClusterWithStats> clustering = getGroups(edges);

        // Update LVs for STs in each group and set group id.
        Iterator<GOeBurstClusterWithStats> gIter = clustering.iterator();
        int gid = 0;
        while (gIter.hasNext()) {
            GOeBurstClusterWithStats g = gIter.next();
            g.setID(gid++);
            g.updateVisibleEdges();
        }

        return clustering;
    }

    public int getSTxLV(Identifiable st, int lv) {
        if (lv > GOeBurstClusterWithStats.MAXLV || lv < 0) {
            level = GOeBurstClusterWithStats.MAXLV;
        }

        return stLVs.get(st.getUID()).lv[lv];
    }

    private ArrayList<Edge<GOeBurstNodeExtended>> getEdges() {
        ArrayList<Edge<GOeBurstNodeExtended>> edges = new ArrayList<Edge<GOeBurstNodeExtended>>();
        maxStId = 0;

        Iterator<GOeBurstNodeExtended> uIter = m.iterator();
        while (uIter.hasNext()) {
            GOeBurstNodeExtended u = uIter.next();
            STLV uLV = stLVs.get(u.getUID());

            if (uLV == null) {
                uLV = new STLV();
                stLVs.put(u.getUID(), uLV);
            }

            maxStId = Math.max(maxStId, u.getUID());

            Iterator<GOeBurstNodeExtended> vIter = m.iterator();
            while (vIter.hasNext()) {
                GOeBurstNodeExtended v = vIter.next();

                int diff = (int) m.getDistance(u, v);

                if (u != v && diff <= GOeBurstClusterWithStats.MAXLV) {
                    uLV.lv[diff - 1]++;
                } else {
                    uLV.lv[GOeBurstClusterWithStats.MAXLV]++;
                }

                if (u.getUID() < v.getUID() && diff <= level) {
                    edges.add(new Edge<GOeBurstNodeExtended>(u, v));
                }
            }
        }

        return edges;
    }

    private Collection<GOeBurstClusterWithStats> getGroups(Collection<Edge<GOeBurstNodeExtended>> edges) {
        DisjointSet s = new DisjointSet(maxStId);

        Iterator<Edge<GOeBurstNodeExtended>> eIter = edges.iterator();
        while (eIter.hasNext()) {
        	Edge<GOeBurstNodeExtended> e = eIter.next();
            s.unionSet(e.getU().getUID(), e.getV().getUID());
        }

        TreeMap<Integer, GOeBurstClusterWithStats> groups = new TreeMap<Integer, GOeBurstClusterWithStats>();
        eIter = edges.iterator();
        while (eIter.hasNext()) {
        	Edge<GOeBurstNodeExtended> e = eIter.next();

            int pi = s.findSet(e.getU().getUID());
            GOeBurstClusterWithStats g = groups.get(pi);
            if (g == null) {
                g = new GOeBurstClusterWithStats(this, m);
                groups.put(pi, g);
            }

            g.add(e);
        }

        // Add singletons.
        Iterator<GOeBurstNodeExtended> stIter = m.iterator();
        while (stIter.hasNext()) {
            GOeBurstNodeExtended st = stIter.next();

            int pi = s.findSet(st.getUID());
            if (groups.get(pi) == null) {
                GOeBurstClusterWithStats g = new GOeBurstClusterWithStats(this, m);
                g.add(st);
                groups.put(pi, g);
            }

            ((GOeBurstClusterWithStats) groups.get(pi)).updateMaxLVs(st);
        }

        ArrayList<GOeBurstClusterWithStats> gList = new ArrayList<GOeBurstClusterWithStats>(groups.values());
        Collections.sort(gList);

        return gList;
    }

}
