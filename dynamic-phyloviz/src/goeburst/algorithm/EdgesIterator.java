package goeburst.algorithm;

import java.util.Iterator;
import java.util.TreeSet;

import goeburst.cluster.Edge;
import matrix.OTU;

public class EdgesIterator<T extends OTU> implements Iterator<Edge<T>> {

	private TreeSet<Edge<T>> set;
	private Iterator<Edge<T>> it;

	private Edge<T> current = null;

	public EdgesIterator(TreeSet<Edge<T>> set, T v) {
		this.set = set;
		this.it = this.set == null ? null : this.set.iterator();
	}

	@Override
	public boolean hasNext() {
		if(it != null)
			return it.hasNext();
		return false;
	}

	@Override
	public Edge<T> next() {
		if(hasNext()){
			return it.next();
		}
		return null;

	}

	public Edge<T> next(int level) {
		if (current == null){
			current =  next();
		}
		
		while(current != null && (int) current.getDistance() < level){
			current = next();
		}
		
		if(current != null && (int) current.getDistance() == level){
			Edge<T> n = current;
			current = next();
			return n;
		}
		
		return null;
		
		
	}

}
