package goeburst.run.dynamic;

import java.util.Comparator;

import goeburst.cluster.Edge;
import goeburst.tree.GOeBurstNode;
import matrix.Identifiable;
import matrix.OTUComparator;
import matrix.TaxaPair;
import matrix.dissimilarity.PairwiseDissimilarity;

public class SimpleComparator<T extends GOeBurstNode> implements OTUComparator<T> {

	private final Comparator<TaxaPair<T>> ecmp = new EdgeComparator();
	private final Comparator<T> pcmp = new IdentifiableComparator();
	private final PairwiseDissimilarity<Identifiable> pd;
	private int maxLevel = 0;

	public SimpleComparator(PairwiseDissimilarity<Identifiable> pd) {
		this(pd, 1);
	}
	public SimpleComparator(PairwiseDissimilarity<Identifiable> pd, int maxLevel) {
		this.pd = pd;
		this.maxLevel = maxLevel;
	}

	@Override
	public int compare(T n1, T n2) {
		return pcmp.compare(n1, n2);
	}

	public int compare(Edge<T> n1, Edge<T> n2) {
		return ecmp.compare(n1, n2);
	}

	@Override
	public int compare(TaxaPair<T> t) {
		return compare(t.getU(), t.getV());
	}

	private class IdentifiableComparator implements Comparator<T> {

		@Override
		public int compare(T u, T v) {
			return u.getUID() - v.getUID();
		}
	}

	private class EdgeComparator implements Comparator<TaxaPair<T>> {

		@Override
		public int compare(TaxaPair<T> f, TaxaPair<T> e) {

			int ret = (int) (pd.distance(f.getU().getIdentifiable(), f.getV().getIdentifiable())
					- pd.distance(e.getU().getIdentifiable(), e.getV().getIdentifiable()));

			return ret;
		}
	}

	@Override
	public Comparator<TaxaPair<T>> getEdgeComparator() {
		return ecmp;
	}

	@Override
	public Comparator<T> getProfileComparator() {
		return pcmp;
	}
	
	@Override
	public int maxLevel() {
		return maxLevel;
	}
}
