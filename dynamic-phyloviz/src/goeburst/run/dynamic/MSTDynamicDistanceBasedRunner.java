package goeburst.run.dynamic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import goeburst.cluster.Edge;
import goeburst.cluster.MSTResult;
import goeburst.tree.GOeBurstNode;
import matrix.Identifiable;
import matrix.Matrix;
import matrix.OTUComparator;
import matrix.dissimilarity.PairwiseDissimilarity;
import project.ProjectItem;

public abstract class MSTDynamicDistanceBasedRunner {

	protected Matrix<GOeBurstNode> m;
	protected final OTUComparator<GOeBurstNode> cmp;
	protected MSTResult<GOeBurstNode> prev_ret;

	public MSTDynamicDistanceBasedRunner(MSTResult<GOeBurstNode> prev, PairwiseDissimilarity<Identifiable> pd, OTUComparator<GOeBurstNode> cmp) {
		this.cmp = cmp;
		this.m = new Matrix<>(pd, cmp);
		List<GOeBurstNode> nodes = new ArrayList<>();
		if (prev != null) {
			for (Edge<GOeBurstNode> e : prev.mst) {
				GOeBurstNode u = e.getU(), v = e.getV();
				if (!nodes.contains(u))
					nodes.add(u);
				if (!nodes.contains(v))
					nodes.add(v);
			}
			Collections.sort(nodes, cmp.getProfileComparator());
			for (GOeBurstNode n : nodes) {
				m.add(n);
			}
			this.prev_ret = prev;
		} else {
			System.err.println("Cannot add a new element to an empty tree.");
		}
	}

	public abstract ProjectItem run(GOeBurstNode newNode);
}
