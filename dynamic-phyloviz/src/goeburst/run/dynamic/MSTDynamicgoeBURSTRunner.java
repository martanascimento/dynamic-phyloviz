/*-
 * Copyright (c) 2011, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole combination.
 * 
 * As a special exception, the copyright holders of this library give you
 * permission to link this library with independent modules to produce an
 * executable, regardless of the license terms of these independent modules,
 * and to copy and distribute the resulting executable under terms of your
 * choice, provided that you also meet, for each linked independent module,
 * the terms and conditions of the license of that module.  An independent
 * module is a module which is not derived from or based on this library.
 * If you modify this library, you may extend this exception to your version
 * of the library, but you are not obligated to do so.  If you do not wish
 * to do so, delete this exception statement from your version.
 */

package goeburst.run.dynamic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import goeburst.algorithm.dynamic.MSTDynamicgoeBURSTAlgorithm;
import goeburst.cluster.Edge;
import goeburst.cluster.MSTResult;
import goeburst.tree.GOeBurstMSTResult;
import goeburst.tree.GOeBurstNode;
import matrix.Identifiable;
import matrix.Matrix;
import matrix.OTUComparator;
import matrix.dissimilarity.PairwiseDissimilarity;
import project.ProjectItem;

public class MSTDynamicgoeBURSTRunner extends MSTDynamicDistanceBasedRunner{

	public MSTDynamicgoeBURSTRunner(MSTResult<GOeBurstNode> prev, PairwiseDissimilarity<Identifiable> pd, OTUComparator<GOeBurstNode> cmp) {
		super(prev, pd, cmp);
	}

	@Override
	public ProjectItem run(GOeBurstNode newNode) {
		int maxLV = cmp.maxLevel();

		MSTResult<GOeBurstNode> ret = null;
		MSTDynamicgoeBURSTAlgorithm<GOeBurstNode> algorithm = new MSTDynamicgoeBURSTAlgorithm<GOeBurstNode>(m, prev_ret.mst);

		if (algorithm.exists(newNode.getID())) {
			System.err.println(
					"Duplicated profile (" + newNode.getIdentifiable() + ")! You can't had the same profile twice.");
			System.exit(0);
		}
		newNode.updateLVs(m, maxLV);
		m.add(newNode);

		List<Edge<GOeBurstNode>> newEdges = new ArrayList<>();
		Iterator<GOeBurstNode> in = m.iterator();
		while (in.hasNext()) {
			GOeBurstNode n = (GOeBurstNode) in.next();
			if (newNode.equals(n))
				continue;
			n.updateLV(m, newNode);
			Edge<GOeBurstNode> e = new Edge<GOeBurstNode>(n, newNode);
			e.setDistance(m.getDistance(e));
			newEdges.add(e);
		}
		Collections.sort(newEdges, m.getComparator().getEdgeComparator());

		ret = algorithm.add(newEdges, prev_ret, newNode);

		return new GOeBurstMSTResult(ret, m, maxLV);
	}

	public Matrix<GOeBurstNode> getMatrix() {
		return m;
	}
}
