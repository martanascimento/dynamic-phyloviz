/*-
 * Copyright (c) 2016, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net/>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package project.action;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import goeburst.output.Format;
import project.ProjectItem;

public final class SaveAsProjectAction {

	public void save(Format format, String fname, ProjectItem res) {
		String filename = fname + format.getExtension();
		Scanner sc = new Scanner(System.in);
		try {
			File f = new File(filename);
			while(f.exists() && !f.isDirectory()) { 
				System.out.print("File " + filename +" already exists. Do you want to override it? (y/n) ");
				String resp = sc.next();
				if(resp.equals("y")){
					break;
				} 
				System.out.print("New file name? ");
				filename = sc.next()  + format.getExtension();
				f = new File(filename);;
			}
			try (PrintWriter pw = new PrintWriter(f)) {
				pw.print(res.getOutput(format));
			}
			System.out.println("\noutput-file: " + f.getAbsolutePath());

		} catch (IOException ie) {
			System.err.println(ie);
		} finally{
			sc.close();
		}
	}
}
