package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import core.data.AbstractProfile;
import core.data.TypingData;
import goeburst.cluster.MSTResult;
import goeburst.output.json.JsonFormat;
import goeburst.output.newick.NewickFormat;
import goeburst.run.dynamic.MSTGenericDynamicDistanceBasedRunner;
import goeburst.run.dynamic.SimpleComparator;
import goeburst.tree.GOeBurstMSTResult;
import goeburst.tree.GOeBurstNode;
import matrix.OTUComparator;
import matrix.dissimilarity.MatrixPairwiseDissimilarity;
import matrix.dissimilarity.PairwiseDissimilarity;
import matrix.dissimilarity.ProfilePairwiseDissimilarity;
import mlst.MLSTypingFactory;
import project.ProjectItem;
import project.action.SaveAsProjectAction;
import snp.SNPFactory;

public class DynamicGenericDistanceBasedApp {
	private static Options getOptions() {
		Options options = new Options();

		options.addOption(Option.builder("t").longOpt("typing-method").hasArg().argName("method")
				.desc("Specify the input data file format. You may choose this method from: (D)istance matrix, (M)LST (default), (S)NP")
				.build())
				.addOption(Option.builder("O").longOpt("output-format").hasArg().argName("format")
						.desc("Specify the output data file format. You may choose this format from: (N)ewick (default) or (J)SON.")
						.build())
				.addOption(Option.builder("o").longOpt("output-tree").hasArg().argName("output tree file")
						.desc("Will write the infered tree into the output tree file.").build())
				.addOption(Option.builder("i").longOpt("input-data").hasArg().argName("input data file")
						.desc("The input data file contains the new typing data or a new entry in a distance matrix.").required().build())
				.addOption(Option.builder("r").longOpt("previous-result").hasArg().argName("previous result data file")
						.desc("The result data file contains a previous computed result.").required().build())
				.addOption(Option.builder("h").longOpt("help").desc("Displays this usage.").build());

		return options;
	}
	public static void main(String[] args) throws IOException, ParseException {
		CommandLineParser parser = new DefaultParser();
		Options options = getOptions();

		CommandLine cmd = parser.parse(options, args);

		if (cmd.hasOption("h")) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(DynamicGenericDistanceBasedApp.class.getSimpleName(), options);
		} else {
			if (!cmd.hasOption("r")) {
				System.err.println("Must use option -r to define your previously computed result data file.");
				return;
			}
			MSTResult<GOeBurstNode> static_result = null;
			try (FileInputStream fileIn = new FileInputStream(new File(cmd.getOptionValue("r")))) {
				try (ObjectInputStream in = new ObjectInputStream(fileIn)) {
					static_result = (MSTResult) in.readObject();
				}

			} catch (IOException | ClassNotFoundException e) {
				System.err.println(e);
			}
			
			if (!cmd.hasOption("i")) {
				System.err.println("Must use option -i to define your input data file.");
				return;
			}
			String in = cmd.getOptionValue("i");
			String out = cmd.hasOption("o") ? cmd.getOptionValue("o")
					: in.substring(in.lastIndexOf("/") + 1, in.lastIndexOf('.'));
			
			FileReader fr = new FileReader(new File(System.getProperty("user.dir"), in));
			PairwiseDissimilarity pd = null;
			TypingData<? extends AbstractProfile> td = null;
			ProjectItem result = null;
			
			
			if (cmd.hasOption("t")) {
				switch (cmd.getOptionValue("t")) {
				case "D":
					pd = new MatrixPairwiseDissimilarity<>(fr, true);
					break;
				case "M":
					td = new MLSTypingFactory().loadData(fr);
					pd = new ProfilePairwiseDissimilarity<>(td);
					break;
				case "S":
					td = new SNPFactory().loadData(fr);
					pd = new ProfilePairwiseDissimilarity<>(td);
					break;
				}
			} else {
				td = new MLSTypingFactory().loadData(fr);
				pd = new ProfilePairwiseDissimilarity<>(td);
			}
			
			GOeBurstNode newNode = new GOeBurstNode(pd.get(0), static_result.size());

			OTUComparator<GOeBurstNode> cmp = new SimpleComparator<>(pd);
			result =  new MSTGenericDynamicDistanceBasedRunner(static_result, pd, cmp).run(newNode);

			SaveAsProjectAction saver = new SaveAsProjectAction();
			if (cmd.hasOption("O")) {
				switch (cmd.getOptionValue("O")) {
				case "J":
					saver.save(new JsonFormat(), out, result);
					break;
				case "N":
					saver.save(new NewickFormat(), out, result);
					break;

				}
			} else {
				saver.save(new NewickFormat(), out, result);
			}

			try (FileOutputStream fileOut = new FileOutputStream(out + ".result")) {
				try (ObjectOutputStream oos = new ObjectOutputStream(fileOut)) {

					oos.writeObject(((GOeBurstMSTResult) result).getResult());
					oos.close();

				}
			} catch (Exception e) {
				System.err.println(e);
			}
		}
	}

}
